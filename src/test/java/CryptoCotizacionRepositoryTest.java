
import jodd.http.HttpException;
import org.json.JSONException;
import org.junit.Test;
import static org.junit.Assert.*;

public class CryptoCotizacionRepositoryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        CryptoCotizacionRepository instance = new CryptoCotizacionRepository();
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = JSONException.class)
    public void obtenerCotizacion_urlIncorrecta_errorHttpException() {
        CryptoCotizacionRepository instance = new CryptoCotizacionRepository();
        instance.setUrl("https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USDasdasd");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlVacia_errorHttpException() {
        CryptoCotizacionRepository instance = new CryptoCotizacionRepository();
        instance.setUrl("");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

}

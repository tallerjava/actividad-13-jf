
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionServiceTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        ArrayList<ProveedorCotizacionRepository> cotizacionServiceListado = new ArrayList();
        cotizacionServiceListado.add(new CoinDeskCotizacionRepository());
        cotizacionServiceListado.add(new BinanceCotizacionRepository());
        cotizacionServiceListado.add(new SomosPNTCotizacionRepository());
        cotizacionServiceListado.add(new CryptoCotizacionRepository());
        ArrayList<ReporteCotizacion> reporteCotizacionListado = new ArrayList();
        CotizacionService service = new CotizacionService();
        ArrayList<ReporteCotizacion> resultadoObtenido = service.obtenerReporteCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_servidorNoResponde_precioEnCero() {
        ArrayList<ProveedorCotizacionRepository> cotizacionServiceListado = new ArrayList();
        CoinDeskCotizacionRepository coinDesk = new CoinDeskCotizacionRepository();
        coinDesk.setUrl("https://api.coindes.com/v1/bpi/currentprice.json");
        cotizacionServiceListado.add(coinDesk);
        CotizacionService service = new CotizacionService();
        double resultadoObtenido = service.obtenerReporteCotizacion.get(0).getPrecio();
        assertEquals(resultadoObtenido, 0);
    }

}

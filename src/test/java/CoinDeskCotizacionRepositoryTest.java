
import jodd.http.HttpException;
import org.json.JSONException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CoinDeskCotizacionRepositoryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = JSONException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        instance.setUrl("https://api.coindesk.com/v1/bpi/currentprice.jsonttttttt");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlVacia_errorHttpException() {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        instance.setUrl("");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

}

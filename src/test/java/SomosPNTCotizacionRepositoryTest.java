
import jodd.http.HttpException;
import org.json.JSONException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class SomosPNTCotizacionRepositoryTest {

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlIncorrecta_errorJSONException() {
        SomosPNTCotizacionRepository instance = new SomosPNTCotizacionRepository();
        instance.setUrl("http://dev.somospnt.com:9756/quotee");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlVacia_urlcorrecta() {
        SomosPNTCotizacionRepository instance = new SomosPNTCotizacionRepository();
        instance.setUrl("");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

}


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cotizacion {

    private double precio;
    private String moneda;
    private Date fechaHora;

    public Cotizacion(Double precio, String moneda, Date fechaHora) {
        this.fechaHora = fechaHora;
        this.moneda = moneda;
        this.precio = precio;
    }

    public double getPrecio() {
        return this.precio;
    }

    public String getMoneda() {
        return this.moneda;
    }

    public Date getFechaHora() {
        return this.fechaHora;
    }

    public void setPrecio(double precio) {
    }

    public void setFechaHora(Date fechaHora) {
    }

    public void setMoneda(String moneda) {
    }

    @Override
    public String toString() {
        DecimalFormat formatoDecimal = new DecimalFormat(".##");
        SimpleDateFormat formateador = new SimpleDateFormat("d-M-yyyy hh:mm");
        return formateador.format(fechaHora) + " / " + this.moneda + " " + formatoDecimal.format(precio);
    }

}

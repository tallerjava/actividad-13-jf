
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class BinanceCotizacionRepository extends ProveedorCotizacionRepository {

    private String url = "https://api.binance.com/api/v1/ticker/price?symbol=BTCUSDT";
    private String nombreProveedor = "Binance";

    @Override
    public Cotizacion obtenerCotizacion() {
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject campo = new JSONObject(response.body());
        double precio = campo.getDouble("price");
        Date fechaHora = new Date();
        String moneda = "USD";
        Cotizacion cotizacion = new Cotizacion(precio, moneda, fechaHora);
        return cotizacion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

}

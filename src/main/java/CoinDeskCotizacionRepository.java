
import java.util.Date;
import jodd.http.HttpRequest;
import org.json.JSONObject;

public class CoinDeskCotizacionRepository extends ProveedorCotizacionRepository {

    private String url = "https://api.coindesk.com/v1/bpi/currentprice.json";
    private String nombreProveedor = "CoindDesk";

    @Override
    public Cotizacion obtenerCotizacion() {
        jodd.http.HttpResponse response = HttpRequest.get(url).send();
        JSONObject campo = new JSONObject(response.body());
        String moneda = campo.getJSONObject("bpi").getJSONObject("USD").getString("code");
        double precio = campo.getJSONObject("bpi").getJSONObject("USD").getDouble("rate_float");
        Date fechaHora = new Date();
        Cotizacion cotizacion = new Cotizacion(precio, moneda, fechaHora);
        return cotizacion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

}


import java.util.ArrayList;

public class CotizacionService {

    private ArrayList<ProveedorCotizacionRepository> cotizacionServiceListado;
    public ArrayList<Cotizacion> obtenerReporteCotizacion;

    /* almacena el contenido de los repositorios*/
    public ArrayList<ReporteCotizacion> obtenerReporteCotizacion() {
        ArrayList<ProveedorCotizacionRepository> cotizacionServiceListado = new ArrayList();
        cotizacionServiceListado.add(new CoinDeskCotizacionRepository());
        cotizacionServiceListado.add(new BinanceCotizacionRepository());
        cotizacionServiceListado.add(new SomosPNTCotizacionRepository());
        cotizacionServiceListado.add(new CryptoCotizacionRepository());
        ArrayList<ReporteCotizacion> reporteCotizacionListado = new ArrayList();
        for (ProveedorCotizacionRepository cotizacionRepository : cotizacionServiceListado) {
            String nombreProveedor = cotizacionRepository.getNombreProveedor();
            try {
                reporteCotizacionListado.add(new ReporteCotizacion(nombreProveedor, cotizacionRepository.obtenerCotizacion()));
            } catch (Exception e) {
                System.out.println(nombreProveedor + ":"+"\t"+"El proveedor no se encuentra disponible");
            }
        }
        return reporteCotizacionListado;
    }
}

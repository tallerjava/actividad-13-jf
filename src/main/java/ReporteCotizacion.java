
public class ReporteCotizacion {

    private String nombreProveedor;
    private Cotizacion cotizacion;

    ReporteCotizacion(String nombreProveedor, Cotizacion cotizacion) {
        this.nombreProveedor = nombreProveedor;
        this.cotizacion = cotizacion;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public Cotizacion getCotizacion() {
        return cotizacion;
    }

    @Override
    public String toString() {
        return nombreProveedor + ": " + cotizacion;
    }

    public void setCotizacion(Cotizacion cotizacion) {
        this.cotizacion = cotizacion;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }
}

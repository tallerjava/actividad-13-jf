
import java.util.ArrayList;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {

        System.out.println("Pulse la tecla Enter para obtener la ultima cotizacion del dia");
        Scanner tecla = new Scanner(System.in);
        String enterKey = tecla.nextLine();
        String receivedKey = null;
        while (enterKey.isEmpty()) {
            CotizacionService cotizacionService = new CotizacionService();
            ArrayList<ReporteCotizacion> reporteCotizacionListado = cotizacionService.obtenerReporteCotizacion();
            for (ReporteCotizacion reporteCotizacion : reporteCotizacionListado) {
                String resultado = reporteCotizacion.getNombreProveedor() + ":" + "\t" + reporteCotizacion.getCotizacion();
                System.out.println(resultado);
            }
            System.out.println("\n" + "(Presione Enter para volver a consultar)" + "\n");
            receivedKey = tecla.nextLine();
        }
    }
}

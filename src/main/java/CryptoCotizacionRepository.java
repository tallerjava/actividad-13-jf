
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class CryptoCotizacionRepository extends ProveedorCotizacionRepository {

    private String url = "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD";
    private String nombreProveedor = "Crypto Compare";

    @Override
    public Cotizacion obtenerCotizacion() {
        Cotizacion cotizacion;
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject campo = new JSONObject(response.body());
        double cotizacionBitcoin = campo.getDouble("USD");
        String moneda = "USD";
        Date fecha = new Date();
        cotizacion = new Cotizacion(cotizacionBitcoin, moneda, fecha);
        return cotizacion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }
}

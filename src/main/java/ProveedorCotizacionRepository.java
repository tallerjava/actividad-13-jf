
public abstract class ProveedorCotizacionRepository {

    public abstract Cotizacion obtenerCotizacion();

    public abstract String getNombreProveedor();

}

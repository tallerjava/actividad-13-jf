
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class SomosPNTCotizacionRepository extends ProveedorCotizacionRepository {

    private String url = "http://dev.somospnt.com:9756/quote";
    private String nombreProveedor = "SomosPNT";

    @Override
    public Cotizacion obtenerCotizacion() {
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject campo = new JSONObject(response.body());
        double precio = campo.getDouble("price");
        String moneda = "USD";
        Date fechaHora = new Date();
        Cotizacion cotizacion = new Cotizacion(precio, moneda, fechaHora);
        return cotizacion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }
}
